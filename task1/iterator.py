import json

def file_iter(name):
    with open(name) as train:
        for line in train:
            yield json.loads(line)

line_no = 0
for jsn in file_iter('train.json'):
    if line_no % 100000 == 0:
        print(jsn)
    line_no += 1
