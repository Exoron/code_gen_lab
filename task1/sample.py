f1 = open('sample_1p.json', 'w')
f3 = open('sample_3p.json', 'w')
f10 = open('sample_10p.json', 'w')

line_no = 0
with open('train.json') as train:
    for line in train:
        if line_no % 100 == 0:
            f1.write(line)
        if line_no % 100 < 3:
            f3.write(line)
        if line_no % 100 < 10:
            f10.write(line)
        line_no += 1

f1.close()
f3.close()
f10.close()
